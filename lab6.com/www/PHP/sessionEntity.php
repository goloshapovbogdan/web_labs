<?php

class sessionEntity
{
    private $DBname = null;
    private $host = null;
    private $DBlogin = null;
    private $DBpass = null;

    function __construct($host, $DBlogin, $DBpass, $DBname)
    {
        $this->DBname = $DBname;
        $this->host = $host;
        $this->DBlogin = $DBlogin;
        $this->DBpass = $DBpass;
    }

    public function DBConnect()
    {
        if ($DBObj = new mysqli($this->host, $this->DBlogin, $this->DBpass, $this->DBname)) {
            return $DBObj;
        } else {
            echo "<div id='sinfo'>Error connection!</div>";
        }
    }

    public function UsersList()
    {
        $userList = null;
        if ($res = $this->DBConnect()->query("SELECT * FROM users")) {
            $userNumber = 0;
            while ($row = $res->fetch_assoc()) {
                $userObj['id'] = $row['id'];
                $userObj['login'] = $row['login'];
                $userObj['pass'] = $row['pass'];
                $userObj['email'] = $row['email'];
                if ($row['perm'] == 1) {
                    $userObj['perm'] = 'admin';
                } else {
                    $userObj['perm'] = 'user';
                }
                $userList[$userNumber] = $userObj;
                $userNumber++;
            }
        }
        return $userList;
    }


    public function PhonesList()
    {
        $phonesSection = null;
        $commentObj = null;
        if ($query = mysqli_query($this->DBConnect(), "SELECT comment.comment,  comment.phoneid, comment.id FROM phonestorage JOIN comment ON phonestorage.id=comment.phoneid")) {
            $commentNum = 0;

            while ($res = $query->fetch_assoc()) {
                $commentObj['comment-' . $commentNum] = $res['comment'];
                $commentObj['phoneid-' . $commentNum] = $res['phoneid'];
                $commentObj['comid-' . $commentNum] = $res['id'];
                $commentNum++;
            }
        }

        if ($res = $this->DBConnect()->query("SELECT * FROM phonestorage")) {
            $phoneNumber = 1;
            while ($row = $res->fetch_assoc()) {
                $commCounter = 0;
                $phoneObj['id'] = $row['id'];
                $phoneObj['name'] = $row['name'];
                $phoneObj['price'] = $row['price'];
                $phoneObj['imgpath'] = $row['imgpath'];
                $phoneObj['descr'] = $row['descr'];
                $phoneObj['diagonal'] = $row['diagonal'];
                $phoneObj['color'] = $row['color'];
                $phoneObj['viewcount'] = $row['viewcount'];

                $phonesSection[$phoneNumber] = $phoneObj;
                $resCommentObj = null;
                for ($i = 0; $i < $commentNum; $i++) {

                    if ($phoneObj['id'] == $commentObj['phoneid-' . $i]) {
                        $resCommentObj['com-' . $i] = 'Комментарий ' . $commCounter . ' (ID = ' . $commentObj['comid-' . $i] . "): " . $commentObj['comment-' . $i];
                        $commCounter++;
                    }
                    $phonesSection[$phoneNumber]['comment'] = $resCommentObj;
                }
                $phoneNumber++;
            }
        }
        return $phonesSection;
    }


    public function CatList($catid = null)
    {
        $phonesSection = null;
        $commentObj = null;
        if ($query = mysqli_query($this->DBConnect(), "SELECT comment.comment,  comment.phoneid, comment.id FROM phonestorage JOIN comment ON phonestorage.id=comment.phoneid")) {
            $commentNum = 0;
            while ($res = $query->fetch_assoc()) {
                $commentObj['comment-' . $commentNum] = $res['comment'];
                $commentObj['phoneid-' . $commentNum] = $res['phoneid'];
                $commentObj['comid-' . $commentNum] = $res['id'];
                $commentNum++;
            }
        }
        if ($res = $this->DBConnect()->query("SELECT * FROM phonestorage")) {
            $phoneNumber = 0;
            while ($row = $res->fetch_assoc()) {
                if ($catid == $row['categoryid']) {
                    $commCounter = 0;
                    $phoneObj['id'] = $row['id'];
                    $phoneObj['name'] = $row['name'];
                    $phoneObj['price'] = $row['price'];
                    $phoneObj['imgpath'] = $row['imgpath'];
                    $phoneObj['descr'] = $row['descr'];
                    $phoneObj['diagonal'] = $row['diagonal'];
                    $phoneObj['color'] = $row['color'];
                    $phoneObj['viewcount'] = $row['viewcount'];
                    $phonesSection[$phoneNumber] = $phoneObj;
                    $resCommentObj = null;
                    for ($i = 0; $i < $commentNum; $i++) {
                        if ($phoneObj['id'] == $commentObj['phoneid-' . $i]) {
                            $resCommentObj['com-' . $i] = 'Комментарий ' . $commCounter . ' (ID = ' . $commentObj['comid-' . $i] . "): " . $commentObj['comment-' . $i];
                            $commCounter++;
                        }
                        $phonesSection[$phoneNumber]['comment'] = $resCommentObj;
                    }
                    $phoneNumber++;
                }
            }
        }
        return $phonesSection;
    }

    public function AddPhone($category = null, $name = null, $price = null, $imgpah = null, $decsr = null, $diagonal = null, $color = null, $crcountry = null)
    {
        if ($res = $this->DBConnect()->query("INSERT INTO phonestorage (id, categoryid, name, price, imgpath, descr, diagonal, color, crcounty) VALUES (NULL,'" . $category . "', '" . $name . "', '" . $price . "', '../img/" . $imgpah . "','" . $decsr . "', '" . $diagonal . "', '" . $color . "', '" . $crcountry . "')")) {
            echo "<div id='sinfo'>Вы успешншо добавили товар!</div>";
        } else {
            echo "<div id='sinfo'>Ошибка пиро занесении!</div>";
        }
    }

    public function ChangePhone($chosenid, $category = null, $name = null, $price = null, $imgpath = null, $descr = null, $diagonal = null, $color = null, $crcountry = null)
    {
        $isCorrectId = false;
        if ($res = $this->DBConnect()->query("SELECT id FROM phonestorage")) {
            echo $chosenid;
            while ($row = $res->fetch_assoc()) {
                if ($chosenid == $row['id']) {
                    $isCorrectId = true;
                }
            }
        }
        if ($isCorrectId) {
            echo $name;
            if ($res = $this->DBConnect()->query("UPDATE  phonestorage SET categoryid = '" . $category . "', name = '" . $name . "', price = '" . $price . "', imgpath = '../img/" . $imgpath . "', descr = '" . $descr . "', diagonal = '" . $diagonal . "', color = '" . $color . "', crcounty = '" . $crcountry . "' WHERE id='" . $chosenid . "'")
            ) {
                echo "<div id='sinfo'>Вы успешншо обновили данные  о товаре!</div>";
            } else {
                echo "<div id='sinfo'>Ошибка при обновлении!</div>";
            }
        } else {
            echo "<div id='sinfo'>Нет такого ID!</div>";
        }
    }


    public function DelPhone($chosenid)
    {
        $isCorrectId = false;
        if ($res = $this->DBConnect()->query("SELECT id FROM phonestorage")) {
            echo $chosenid;
            while ($row = $res->fetch_assoc()) {
                if ($chosenid == $row['id']) {
                    $isCorrectId = true;
                }
            }

        }
        if ($isCorrectId) {
            if ($res = $this->DBConnect()->query("DELETE FROM phonestorage WHERE id='" . $chosenid . "'")) {
                echo "<div id='sinfo'>Товар успешно удалён!</div>";
            } else {
                echo "<div id='sinfo'>Нет такого товара!</div>";
            }
        }
    }

    public function GetCategories()
    {
        $cat[] = null;
        if ($res = $this->DBConnect()->query("SELECT categoryid FROM phonestorage")) {
            $i = 0;
            while ($row = $res->fetch_assoc()) {
                $cat[$i] = $row['categoryid'];
                $i++;
            }
            $resultCat = null;
            $lengthResultCat = 0;

            for ($i = 0; $i < count($cat); $i++) {
                $flag = false;
                for ($j = $i; $j < count($cat); $j++) {
                    if ($cat[$i] == $cat[$j] && $j != $i) {
                        $flag = true;
                    }
                }
                if (!$flag) {
                    $resultCat[$lengthResultCat] = $cat[$i];
                    $lengthResultCat++;
                }
            }
            sort($resultCat);
            return $resultCat;
        } else {
            echo "<div id='sinfo'>Ошибка получения категорий!</div>";
        }
    }


    public function DelComment($commid = null)
    {
        $isCorrectId = false;
        if ($res = $this->DBConnect()->query("SELECT id FROM comment")) {
            while ($row = $res->fetch_assoc()) {
                if ($commid == $row['id']) {
                    $isCorrectId = true;
                }
            }
        }

        if ($isCorrectId) {
            if ($res = $this->DBConnect()->query("DELETE FROM comment WHERE id='" . $commid . "'")) {
                echo "<div id='sinfo'>Комментарий успешно удалён!</div>";
            }
        } else {
            echo "<div id='sinfo'>Нет такого комментария!</div>";
        }
    }

    public function AddComment($phoneid = null, $commentText = null)
    {
        $isCorrectId = false;
        if ($res = $this->DBConnect()->query("SELECT id FROM phonestorage")) {
            while ($row = $res->fetch_assoc()) {
                if ($phoneid == $row['id']) {
                    $isCorrectId = true;
                }
            }
        }
        if ($isCorrectId) {
            if ($res = $this->DBConnect()->query("INSERT INTO comment (phoneid, comment) VALUES ('" . $phoneid . "', '" . $commentText . "')")) {
                echo "<div id='sinfo'>Комментарий успешно добавлен!</div>";
            }
        } else {
            echo "<div id='sinfo'>Нет такого товара!</div>";
        }
    }
}

?>


