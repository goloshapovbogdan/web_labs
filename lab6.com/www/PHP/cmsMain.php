<?php
session_start();

if ($_POST['logout'] == 'logout') {
    unset($_SESSION['admin']);
    unset($_SESSION['login']);
}
if (!$_SESSION['admin'] && !$_SESSION['login']) {
    header('Location: loginForm.php');
    exit;
}
?>
    <link rel="stylesheet" href="../CSS/phones-block.css">
    <link rel="stylesheet" href="../CSS/users-block.css">
    <link rel="stylesheet" href="../CSS/add-phones-block.css">
    <link rel="stylesheet" href="../CSS/categories-block.css">
    <link rel="stylesheet" href="../CSS/pageWork.css">
    <script type="text/javascript" src="../JS/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="../JS/ajaxInfo.js"></script>

<?php
include "header.php";
include "footer.php";
include "sessionEntity.php";
include "pageWorkEntity.php";
$session = new sessionEntity("localhost", "root", "", "cms");
$pageWork = new pageWork($_POST['page'] . '.php', $_POST['context']);

if ($_POST['show-products'] == 'products') {
    $phonesList = $session->PhonesList();
    $viewCounter = 0;
    echo "<div class='phones-block'>";
    foreach ($phonesList as $item) {
        echo "<div class='phone'>
                <div class='short-info' id='id-info'>Product ID: " . $item['id'] . "</div>
                <div class='short-info' id='name-info'>Name: " . $item['name'] . "</div>
                <div class='short-info' id='price-info'>Price: " . $item['price'] . " UAH</div><hr>
                <div class='animation-block'><div class='mask'></div>
                <img class='imgcolor' src='" . $item['imgpath'] . "'></div><hr>
                <div class='short-info' id='descr-info'>Desctription : " . $item['descr'] . "</div>
                <div class='short-info' id='diagonal-info'>Diagonal: " . $item['diagonal'] . "</div>
                <div class='short-info' id='color-info'>Country: " . $item['color'] . "</div>";
        if ($item['comment']) {
            echo '<hr>Комментарии:<br>';
            foreach ($item['comment'] as $com) {
                echo "<br><div class ='short-info' id='comment-info'>" . $com . " </div>";
            }
        }
        echo "<hr><button onclick='sendAjax(" . $item['id'] . ")'>Статистика просмотров</button></div>";
    }
}

if ($_POST['show-users'] == 'users') {
    $userList = $session->UsersList();
    $viewCounter = 0;
    $i = 1;
    echo "<div class='phones-block' id='users-block'>";
    foreach ($userList as $item) {
        echo "<div class='user'><div id='nuser'> User: " . $i . "</div><hr>
                <div class='user-info' id='id-info'>ID: " . $item['id'] . "</div><br>
                <div class='user-info' id='email-info'>Email: " . $item['email'] . "</div><br>
                <div class='user-info' id='login-info'>Login: " . $item['login'] . " </div><br>
                <div class='user-info' id='pass-info'>Password: " . $item['pass'] . " </div><br>
                <div class='user-info' id='pass-info'>Permission: " . $item['perm'] . " </div><br>       
                </div>";
        $i++;
    }
    echo "<div>";
}

if ($_POST['show-add-product'] == 'add product') {
    echo "<div class = 'phones-block'></div>";
    echo "<div id='phone-add-block'>
           Форма добавления нового продукта:<br>
           <form action='cmsMain.php' method='POST' enctype='multipart/form-data'> 
           Категория: <input type='text' placeholder='name' name='category'required=''><br>
           Имя: <input type='text' placeholder='name' name='name'required=''><br>
           Цена: <input type='text' placeholder='price' name='price' required=''><br>
           Изображение:  <input type='file' name='imagetoload' id='imagetoload' accept='image/*'><br>
           Описание: <input type='text' placeholder='descr' name='descr' required=''><br>
           Диагональ: <input type='text' placeholder='diagonal' name='diagonal' required=''><br>
           Цвет: <input type='text' placeholder='color' name='color' required=''><br>
           Призводитель: <input type='text' placeholder='crcountry' name='crcountry' required=''><br>
           <input type='submit' value='Add to the storage' name='add-phone' id='add-phone'><br>
</form>
</div>";
}

if ($_POST['add-phone'] == 'Add to the storage') {
    $session->AddPhone($_POST['category'], $_POST['name'], $_POST['price'], $_FILES["imagetoload"]["name"], $_POST['descr'], $_POST['diagonal'], $_POST['color'], $_POST['crcountry']);
}

if ($_POST['show-change-product'] == 'change product') {
    echo "<div class = 'phones-block'>";
    echo "<div id='phone-change-block'>
<div id='phone-inchange-block'>
           Форма изменения продукта:<br>
           <form action='cmsMain.php' method='POST' enctype='multipart/form-data'>
           ID товара для изменения: <input type='text' placeholder='Product ID' name='id'required=''><br>
           Категория: <input type='text' placeholder='name' name='category'><br>
           Имя: <input type='text' placeholder='name' name='name'><br>
           Цена: <input type='text' placeholder='price' name='price'><br>
           Изображение:  <input type='file' name='imagetoload' id='imagetoload' accept='image/*'><br>
           Описание: <input type='text' placeholder='descr' name='descr' ><br>
           Диагональ: <input type='text' placeholder='diagonal' name='diagonal'><br>
           Цвет: <input type='text' placeholder='color' name='color' ><br>
           Призводитель: <input type='text' placeholder='crcountry' name='crcountry'><br>
           <input type='submit' value='update product' name='change-product' id='add-phone'><br> 
</form>
</div>
</div>
</div>";
}

if ($_POST['change-product'] == 'update product') {
    $session->ChangePhone($_POST['id'], $_POST['category'], $_POST['name'], $_POST['price'], $_FILES["imagetoload"]["name"], $_POST['descr'], $_POST['diagonal'], $_POST['color'], $_POST['crcountry']);

}

if ($_POST['show-delete-product'] == 'delete product') {
    echo "<div class = 'phones-block'></div>";
    echo "<div id='phone-del-block'>
           Форма удаления продукта :<br>
           <form action='cmsMain.php' method='POST' enctype='multipart/form-data'>
           ID товара для изменения: <input type='text' placeholder='Product ID' name='id'required=''><br>
           <input type='submit' value='delete product' name='delete-product' id='add-phone'><br> 
</form>
</div></div>";
}

if ($_POST['delete-product'] == 'delete product') {
    $session->DelPhone($_POST['id']);
}

if ($_POST['show-categories'] == 'categories') {
    $arrCat = $session->GetCategories();
    echo "<div class = 'phones-block' id= >";
    echo "<div id='category-block'>   
           <form action='cmsMain.php' method='POST'><br>";

    for ($i = 0; $i < count($arrCat); $i++) {
        echo "<div id='cat-block'><input type='submit' value='" . $arrCat[$i] . "' name='show-cat-" . $arrCat[$i] . "' id='cat-choise'></div>";
    }
    echo '</div></div>';


}

$arrCat = $session->GetCategories();
for ($i = 0; $i < count($arrCat); $i++) {
    if ($_POST["show-cat-" . $arrCat[$i]] == $arrCat[$i]) {
        $_POST['flag'] = 'good';
        $phonesList = $session->CatList($arrCat[$i]);
        $viewCounter = 0;
        echo "<div class = 'phones-block' >";
        echo "<div id='category-block'>   
          <form action='cmsMain.php' method='POST'><br>";

        for ($i = 0; $i < count($arrCat); $i++) {
            echo "<div id='cat-block'><input type='submit' class='cat' value='" . $arrCat[$i] . "' name='show-cat-" . $arrCat[$i] . "' id='cat-choise'></div>";
        }
        echo '</form></div></div>';
        echo "<div class='categoryes-block'>";

        foreach ($phonesList as $item) {
            echo "<div class='phone'>
                <div class='short-info' id='id-info'>Product ID: " . $item['id'] . "</div>
                <div class='short-info' id='name-info'>Name: " . $item['name'] . "</div>
                <div class='short-info' id='price-info'>Price: " . $item['price'] . " UAH</div><hr>
                <div class='animation-block'><div class='mask'></div>
                <img class='imgcolor' src='" . $item['imgpath'] . "'></div><hr>
                <div class='short-info' id='descr-info'>Desctription : " . $item['descr'] . "</div>
                <div class='short-info' id='diagonal-info'>Diagonal: " . $item['diagonal'] . "</div>
                <div class='short-info' id='color-info'>Country: " . $item['color'] . "</div>";
            if ($item['comment']) {
                echo '<hr>Комментарии:<br>';
                foreach ($item['comment'] as $com) {
                    echo "<br><div class ='short-info' id='comment-info'>" . $com . " </div>";
                }
            }
            echo "<hr><button onclick='sendAjax(" . ($item['id']) . ")'>Статистика просмотров</button></div>";
        }
    }
}

if ($_POST['show-delete-comment'] == 'delete comment') {
    echo "<div class = 'phones-block'></div>";
    echo "<div id='com-del-block'>
           Форма удаления комметария :<br>
           <form action='cmsMain.php' method='POST'>
           ID комментария для удаления: <input type='text' placeholder='Comment ID' name='comment-id'required=''><br>
           <input type='submit' value='delete comment' name='delete-comment' id='add-phone'><br> 
</form>
</div>";
}
if ($_POST['delete-comment'] == 'delete comment') {
    $session->DelComment($_POST['comment-id']);
}

if ($_POST['show-add-comment'] == 'add comment') {
    echo "<div class = 'phones-block'></div>";
    echo "<div id='phone-add-block'>
           Форма добавления комметария :<br>
           <form action='cmsMain.php' method='POST'>
           ID продукта для добавления: <input type='text' placeholder='Product ID' name='product-id'required=''><br>
           <textarea placeholder='Text' name='text'  cols='30' rows='10'></textarea>
           <input type='submit' value='add comment' name='add-comment' id='add-phone'><br> 
</form>
</div>";
}

if ($_POST['add-comment'] == 'add comment') {
    $session->AddComment($_POST['product-id'], $_POST['text']);
}


if ($_POST['show-create-page'] == 'create page') {
    echo "<div class = 'phones-block'></div>";
    echo "<div id='page-add-block'>
           Форма добавления новой страницы :<br>
           <form action='cmsMain.php' method='POST'>
           Введите имя: <input type='text' placeholder='Page name' name='page'  pattern='[^./\]{1,20}' required=''><br> 
          <textarea  placeholder='Text' name='context'  cols='30' rows='10'></textarea><br>
           <input type='submit' name='create-page' value='create page'><br> 
</form>
</div>";
}

if ($_POST['create-page'] == 'create page') {
    $pageWork->createPage();
}

if ($_POST['show-delete-page'] == 'delete page') {
    echo "<div class = 'phones-block'></div>";
    echo "<div id='page-del-block'>
           Форма удаления страницы :<br>
           <form action='cmsMain.php' method='POST'>
           Введите имя: <input type='text' placeholder='Page name' name='page'  pattern='[^./\]{1,20}' required=''><br>
           <input type='submit' name='delete-page' value='delete page'><br> 
</form>
</div>";
}

if ($_POST['delete-page'] == 'delete page') {
    $pageWork->deletePage();
}

?>