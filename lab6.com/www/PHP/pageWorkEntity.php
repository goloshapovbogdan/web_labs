<?php

class pageWork
{
    public $name = null;
    public $context = null;

    function __construct($name, $context)
    {
        $this->name = $name;
        $this->context = $context;
    }

    function createPage()
    {
        if ($fp = fopen($this->name, "w")) {
            echo "<div id='sinfo'>Всё прошло ровно!</div>";
            fwrite($fp, $this->context);
            fclose($fp);//
        }
        else {
            echo "<div id='sinfo'>Ошибка создания файла!</div>";
        }
    }

    function deletePage()
    {
        if (file_exists($this->name)) {
            unlink($this->name);
        } else {
            echo '<div id=\'sinfo\'>Нет такого файла!</div>';
        }
    }
}

?>