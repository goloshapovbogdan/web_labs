<?php

class authenticationSystemEntity
{
    public $email = null;
    public $login = null;
    public $pass = null;

    function __construct($email, $login, $pass)
    {
        $this->email = $email;
        $this->login = $login;
        $this->pass = $pass;
    }

    public function Autorization($url)
    {
        session_start();
        $error = true;
        if ($_POST['submit']) {
            $objBD = new mysqli("localhost", "root", "", "cms");
            if ($res = $objBD->query("SELECT login, pass, perm FROM users")) {
                while ($row = $res->fetch_assoc()) {
                    if ($this->login == $row['login'] && md5(md5($this->pass)) == $row['pass']) {
                        $error = false;
                        $_SESSION['login'] = $this->login;
                        if ($row['perm'] == 1) {
                            $_SESSION['admin'] = 'admin';
                        }

                        header("Location:".$url);
                        exit();
                    }
                }
                if ($error) {
                    print "<div id='err'>Вы ввели неправильный логин/пароль</div>";
                }
            }
        }
    }


    public function Registration()
    {
        $err = null;
        if ($objBD = new mysqli("localhost", "root", "", "cms")) {
            if (isset($_POST['submit'])) {
                // проверям логин
                if (!preg_match("/^[a-zA-Z0-9]+$/", $this->login)) {
                    $err[] = "Логин может состоять только из букв английского алфавита и цифр";
                }

                if (strlen($this->login) < 3 or strlen($this->login) > 30) {
                    $err[] = "Логин должен быть не меньше 3-х символов и не больше 30";
                }


                if (!preg_match("/^[a-z0-9](([_.-]?[a-z0-9]+)*){0,19}@([a-z0-9]([_-]?[a-z0-9]+)?.){1,3}[a-z]{2,6}$/i", $this->email)) {
                    $err[] = "Некорректный еmail";
                }

                if ($res = $objBD->query("SELECT login FROM users")) {
                    while ($row = $res->fetch_assoc()) {
                        {
                            if ($this->login == $row['login']) {
                                $err[] = "Пользователь с таким логином уже существует в базе данных";
                            }
                        }
                    }
                }
                if (count($err) == 0) {
                    $login = $this->login;
                    $password = md5(md5(trim($this->pass)));
                    $email = $this->email;
                    mysqli_query($objBD, "INSERT INTO users SET email ='" . $email . "', login='" . $login . "', pass='" . $password . "'");
                    $error = false;
                    header("Location: loginForm.php");
                    exit();
                } else {
                    print "<b>При регистрации произошли следующие ошибки:</b><br>";
                    foreach ($err AS $error) {
                        print $error . "<br>";
                    }
                }
            }
        }
    }
}

?>