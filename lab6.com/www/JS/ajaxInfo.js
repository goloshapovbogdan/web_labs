function sendAjax(tmp) {
    $.ajax({
        url: "ajaxWork.php?id=" + tmp,
        success: function (data, textStatus, jqXHR,e) {
            var jsonObj = JSON.parse(data);
            if (jsonObj != null) {
                $('#aboutinfo').html('')
                for (var i = 0; i < jsonObj.length; i++) {
                    $('#aboutinfo').append("<div class='date'>Date: " + jsonObj[i]['date'] + ", views: " + jsonObj[i]['viewcount'] +"</div><hr>")
                    $('#back-phone, #aboutinfo, #exitbut').css('display', 'block');
                }

                $('#exitbut').bind('click', function () {
                    $('#back-phone,#aboutinfo, #exitbut').css('display', 'none');
                });
            }
            return false; //отключаем реакцию на ссылку
            event.preventDefault(e);
        },
        error: function (jqXHR, textStatus, errorThrown) {

            $(body).html('Ошибка при загрузке...');
        },
        beforeSend: function (xhr) {

            $('body').append('<div id="sinfo">Подождите...</div>');
        },
        complete: function (jqXHR, textStatus) {
            $('#sinfo').remove();
        }
    });
}