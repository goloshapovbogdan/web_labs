$(document).ready(function () {
    var parse_res;
    $.ajax({
        type: "GET",
        url: '../PHP/ajaxParser.php',
        success: function (res) {
            parse_res = JSON.parse(res);
            Canvas(parse_res);
        },
        error: function () {
            alert('Error!')
        }
    });


    $('#choose-file').submit(function () {
        if ($('#width-img').val() == '' || $('#height-img').val() == '' || $('#open-file').val() == '') {
            alert('Input values!');
            event.preventDefault();
        }
    })

});


function Canvas(parse_res) {
    maxEl = 250;
    var widthLine = 790;
    var canvas = document.getElementById("canv");
    var context = canvas.getContext("2d");
    var arr = ["", "red", "blue", "black", "orange", "#0545D6", "#14AD14", "#FFC000", "#00FFEA", "#FC00FF"];
    context.fillStyle = "#fff";
    context.fillRect(0, 0, 845, 490);
    context.clearRect(3, 3, 841, 463);
    context.moveTo(30, 30);
    context.lineTo(30, 225);
    context.strokeStyle = "#000";
    context.lineWidth = 3;
    context.stroke();
    context.fillStyle = "#000";
    context.font = "bold 10px Consolas";
    context.beginPath();
    var maxVal = Math.max.apply(null, parse_res);
    var minVal = Math.min.apply(null, parse_res);
    var diapazon = maxVal - minVal;
    var zeroLvl = 225;
    if (minVal < 0) {
        zeroLvl = 30 + (195 / diapazon) * maxVal;
        context.fillText(0, 10, zeroLvl + 3)
    }
    var withRect = 445 / (parse_res.length * 2);


    for (i = 0; i < 6; i++) {
        context.lineWidth = 1;
        context.moveTo(27, 30 + 195 / 5 * i);
        context.lineTo(33 , 30 + 195 / 5 * i);//195 длина по oy
        context.stroke();

        if (minVal < 0) {
            context.fontSize = 10;
            context.fillText(maxVal - diapazon / 5 * i, 10, 30 + 195 / 5 * i + 3);
        }
        else {
            context.fillText(maxVal - maxVal / 5 * i, 10, 30 + 195 / 5 * i + 3);
        }
    }

    context.moveTo(27, zeroLvl);
    context.lineTo(475, zeroLvl);//195 длина по oy
    context.stroke();
    context.fillStyle = "#00f";
    for (i = 0; i < parse_res.length; i++) {
        context.lineWidth = 1;
        context.moveTo(30 + 445 / parse_res.length * i, zeroLvl - 3);
        context.lineTo(30 + 445 / parse_res.length * i, zeroLvl + 3);//195 длина по oy
        context.stroke();
        if (minVal < 0) {
            if (parse_res[i] >= 0) {
                context.fillRect(30 + 445 / parse_res.length * i + withRect / 2, zeroLvl, withRect, -195 / diapazon * parse_res[i]);
                context.fillText(parse_res[i], 30 + 445 / parse_res.length * i + withRect / 2 + 10, -8 + zeroLvl - 195 / diapazon * parse_res[i] + 5);
            }
            else {
                context.fillRect(30 + 445 / parse_res.length * i + withRect / 2, zeroLvl, withRect, -195 / diapazon * parse_res[i]);
                context.fillText(parse_res[i], 30 + 445 / parse_res.length * i + withRect / 2 + 10, 5 + zeroLvl - 195 / diapazon * parse_res[i] + 5);
            }
        }
        else {
            context.fillRect(30 + 445 / parse_res.length * i + withRect / 2, zeroLvl, withRect, -195 / maxVal * parse_res[i]);
            context.fillText(parse_res[i], 30 + 445 / parse_res.length * i + withRect / 2 + 10, zeroLvl - 195 / maxVal * parse_res[i] - 5);
        }
    }
}


//     var $myCanvas = $('#canv');
//
// $myCanvas.drawLine({
//     strokeStyle: 'steelblue',
//     strokeWidth: 3,
//     x1: 45, y1: 40,
//     x2: 45, y2: 40 + oyLine,
//     x3: 45 + oxLine + 1, y3: 210});
//
//
// for(var i = 0; i <= 5; i++) {
//     $myCanvas.drawLine({
//         strokeStyle: 'steelblue',
//         strokeWidth: 1,
//         x1: 40, y1: 40 + i* oyLine / 5,
//         x2: 45 + oxLine, y2: 40 + i* oyLine / 5
//     }).drawText({text: (maxValue/5*i).toFixed(),
//         fontFamily: 'cursive',
//         fontSize: 10,
//         x: 35, y: 40 - i*oyLine/5  + oyLine,
//         fillStyle: 'lightblue',
//         strokeStyle: 'blue',
//         strokeWidth: 1
//
//     })
// }
// var cntOxSpace = oxLine/colCount
// var curOxPoint = 45;//start point OX
// var widthColumn = cntOxSpace/2.5;
// var heightColumtCoef = oyLine/maxValue;
//
// for(var i = 0; i < colCount; i++)
// {
//
//     $myCanvas.drawRect({
//         fillStyle: 'steelblue',
//         strokeStyle: 'blue',
//         strokeWidth: 4,
//         x: curOxPoint + cntOxSpace/2 - widthColumn/2, y: 40 + oyLine,
//         fromCenter: false,
//         width: widthColumn,
//         height: -(jsonObj[i]*heightColumtCoef)
//     });
//
//     $myCanvas.drawText({
//         text: jsonObj[i],
//         fontFamily: 'cursive',
//         fontSize: 10,
//         x: curOxPoint +cntOxSpace/2, y: 50 + oyLine -(jsonObj[i]*heightColumtCoef + 20) ,
//         fillStyle: 'lightblue',
//         strokeStyle: 'blue',
//         strokeWidth: 1
//     }).drawText({
//         text: i + 1,
//         fontFamily: 'cursive',
//         fontSize: 10,
//         x: curOxPoint +cntOxSpace/2, y: 50 + oyLine,
//         fillStyle: 'lightblue',
//         strokeStyle: 'blue',
//         strokeWidth: 1
//     });
//
//     curOxPoint += cntOxSpace;
//
//     $myCanvas.drawLine({ strokeStyle: 'black',
//         strokeWidth: 1,
//         x1: curOxPoint , y1: 40 + oyLine,
//         x2: curOxPoint , y2: 45 + oyLine})
//
//
// }






