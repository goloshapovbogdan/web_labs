function sendAjax(numOfPhone){
	var xhr = new XMLHttpRequest();
	xhr.open('GET', '../lab0.xml', true);
	xhr.send();

	xhr.onreadystatechange = function(){
		if(xhr.readyState == 4 && xhr.status == 200)
		{
			var response = xhr.responseXML;
			var productlist = response.getElementsByTagName("productlist")[0];
			var phones = productlist.getElementsByTagName("phone");
			 if(phones)
			 {
			 		var phone = phones[numOfPhone];
			 		var aserialn = phone.getElementsByTagName('serialn')[0].childNodes[0].nodeValue;
			 		var acryear= phone.getElementsByTagName('cryear')[0].childNodes[0].nodeValue;
			 		var acrcountry = phone.getElementsByTagName('crcountry')[0].childNodes[0].nodeValue;
			 		var adiagonal = phone.getElementsByTagName('diagonal')[0].childNodes[0].nodeValue;
			 		var acolor = phone.getElementsByTagName('color')[0].childNodes[0].nodeValue;
			 		var aimg = phone.getElementsByTagName('img')[0].getAttribute("description");
			 		var resstr = "Seria: " + aserialn + "<br>Year: " + acryear + "<br>Country: " + acrcountry + "<br>Diagonal: " + adiagonal + "<br>Color:" + acolor;
			 		if(aimg != null)
			 		{
		 				resstr +=  "<br>Description: " + aimg;
			 		}
			 	
			 		document.getElementById("context").style.opacity = 0.9;
			 		document.getElementById("maintext").innerHTML = resstr;
			 	
			}
			else {
				alert('Ajax request failed');
			}
		}
	}
}

function closeForm(){
	document.getElementById("context").style.opacity = 0;
}