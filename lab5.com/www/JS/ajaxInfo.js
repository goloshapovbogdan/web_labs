function sendAjax(tmp) {
    $.ajax({
        url: "../PHP/dbWork.php?id=" + tmp,
        success: function (data) {
            var jsonObj = JSON.parse(data);

            if (jsonObj != null) {
                $('#aboutinfo').html('').append("Serial number: " + jsonObj['serialnumber'] + "<hr>Year: " + jsonObj['cryear'] + "<hr>County: " + jsonObj['crcountry'] + "<hr>Diagonal: " + jsonObj['diagonal'] + "<hr>Color: " + jsonObj['color'] + "<hr>Views: " + jsonObj['viewcount']);
                $('#back-phone, #aboutinfo, #exitbut').css('display', 'block');
            }

            $('#exitbut').bind('click', function () {
                $('#back-phone,#aboutinfo, #exitbut').css('display', 'none');
            });
        }
    });
}