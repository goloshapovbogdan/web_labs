<link rel="stylesheet" href="../CSS/carousel.css">
<script type="text/javascript" src="../JS/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../JS/carousel.js"></script>
<script type="text/javascript" src="../JS/jqAjax.js"></script>
<?php
$xmlpath = "../lab2.xml";
$xml = new DOMDocument();
$xml->load($xmlpath);
$x = $xml->documentElement;
$productlist = $xml->getElementsByTagName("phone");
$name = $xml->getElementsByTagName("img");
$numOfPhone = 0;
?>
<header>
    <div id="name">
        <div id="parent-text">
            MyCarousel
        </div>
        <div id="child-text">
            By B. Goloshapov
        </div>
    </div>
</header>
<main>
    <div id="phone-block">
        <div class="carousel-arrow-left"><</div>
        <div id="carousel">
            <div class="carousel-hider">
                <ul class="carousel-list">
                    <?php
                    foreach ($productlist as $node) {
                        echo "<li class='carousel-element'>";
                        echo $node->getAttribute('name');
                        foreach ($xmp as $value) {
                            if ($value->nodeName == "price")
                                echo "<div id='pricephone'><hr>Price: " . $value->nodeValue . "</div>";
                            if ($value->nodeName == "img") {
                                echo "<div class='parent'><div id='mask'></div></div><img class='imgcolor' src='../" . $value->getAttribute('path') . "'>";
                            }
                        }
                        echo "<br><div class='aboutbutton' onclick='sendAjax(" . $numOfPhone . ")'>Подробнее</div>";
                        $xmp = $node->childNodes;
                        $numOfPhone++;
                        echo "</li>";
                    }
                    ?>
                </ul>
            </div>
        </div>
        <div class="carousel-arrow-right">></div>
    </div>
</main>
<footer>
    <div id="about">
        <div id="company" onclick="">Goloshapov inc.</div>
        <div id="my">About Me</div>
    </div>
    <div id="info">That paje was made form second lab work on WEB-programming course. 2017 year</div>
    <div id="back-phone"></div>
    <div id="aboutinfo"></div>
    <div id="exitbut">X</div>
</footer>