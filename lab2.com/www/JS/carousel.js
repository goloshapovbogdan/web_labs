$(document).ready(function() {

    $('.parent').hover(function() {
        $(this).stop().animate({ opacity: "0"}, 400);
    },function() {
        $(this).stop().animate({ opacity: "0.5" }, 400);
    })

    var leftUIEl = $('.carousel-arrow-left');
    var rightUIEl = $('.carousel-arrow-right');
    var elementsList = $('.carousel-list');

    var offset = $('.carousel-element').outerWidth();
    var currentLeftValue = 0;
    var valueoOfVisibleElements = 3;
    var valueOfAllElements = 10;

    leftUIEl.click(function() {
        if (currentLeftValue < 0) {
            currentLeftValue += offset;
            elementsList.animate({ 'margin-left' : currentLeftValue + "px"}, 500);
        }
    });

    rightUIEl.click(function() {
        if (currentLeftValue > -offset*(valueOfAllElements - valueoOfVisibleElements)) {
            currentLeftValue -= offset;
            elementsList.animate({ 'margin-left' : currentLeftValue + "px"}, 500);
        }
    });


});