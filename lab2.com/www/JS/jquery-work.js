$('document').ready(function () {
    var checkflag = false;
    var checkmail = false, checklog = false, checkpass = false, checkconfpass = false;
    var mailpattern = /^([a-z0-9_\.-])+@[a-z0-9-]+(\.[a-z]{2,4}){1,3}$/i;
    var loginpattern = /^[a-z0-9_\.-]{5,10}$/i;


    $('#email-check').bind('click focus blur keyup', function () {
        if (!mailpattern.test(
                $('#email-check').val())
        ) {
            $('#email-error').css('opacity', '1');
            $('#email-done').css('opacity', '0');
            checkmail = false;
        }
        else {
            $('#email-error').css('opacity', '0');
            $('#email-done').css('opacity', '1');
            checkmail = true;
        }
    });

    $('#login-check').bind('click focus blur keyup', function () {
        if (!loginpattern.test(
                $('#login-check').val())
        ) {
            $('#login-error').css('opacity', '1');
            $('#login-done').css('opacity', '0');
            checklog = false;
        }
        else {
            $('#login-error').css('opacity', '0');
            $('#login-done').css('opacity', '1');
            checklog = true;
        }
    });

    $('#pass-check').bind('click focus blur keyup', function () {
        if (loginpattern.test(
                $('#pass-check').val())
        ) {
            $('#conf-pass-check').attr('readonly', false);
            $('#pass-error').css('opacity', '0');
            $('#pass-done').css('opacity', '1');
            checkpass = true;
        }
        else {
            $('#pass-error').css('opacity', '1');
            $('#pass-done').css('opacity', '0');
            checkpass = false;
            $('#conf-pass-check').attr('readonly', true).val('');
        }
    });

    $('#conf-pass-check').bind('click focus blur keyup', function () {
        if ($('#conf-pass-check').val() == $('#pass-check').val() && $('#conf-pass-check').attr('readonly') != 'readonly') {
            $('#conf-pass-error').css('opacity', '0');
            $('#conf-pass-done').css('opacity', '1');
            checkconfpass = true;
        }
        else {
            $('#conf-pass-error').css('opacity', '1');
            $('#conf-pass-done').css('opacity', '0');
            checkconfpass = false;
        }
    });

    $('#submit-form-on-template').submit(function () {
        if (checkmail && checklog && checkpass && checkconfpass) {
            return true;
        }
        else {
            $('#wind').css('display', 'block');
            $('#exitbut').css('display', 'block');
            $('#incorrect').css('display', 'block')
            return false;
        }
    });

    $('#exitbut').click(function () {
        $('#wind').css('display', 'none');
        $('#exitbut').css('display', 'none');
        $('#incorrect').css('display', 'none');
    })

    $('#set-all').click(function () {

        $('.check').prop( "checked", true);
    })

    $('#remove-all').click(function () {
        $('.check').prop( "checked", false);
    })

    $('#invert-all').click(function () {
        $('#checkblock input:checkbox').each(function () {
            this.checked = !this.checked;
        });
    })
});

