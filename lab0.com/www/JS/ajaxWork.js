function sendAjax(numOfPhone) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'PHP/ajaxParser.php?id=' + numOfPhone, true);
    xhr.send();

    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
			var answString = xhr.responseText;
			var jsonObj = JSON.parse(answString);
            if(jsonObj != null) {
                var resstr = "SerialNumb: " + jsonObj['serialn'] + "<br>Create year: " +  jsonObj['cryear'] + "<br>Country:  " + jsonObj['crcountry'] + "<br>Diagonal: " + jsonObj['diagonal'] + "<br>Color: " +  jsonObj['color'];
                document.getElementById("context").style.opacity = 0.9;
                document.getElementById("maintext").innerHTML = resstr;
            }
        }
    }
}

function closeForm(){
	document.getElementById("context").style.opacity = 0;
}